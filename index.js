// Опишіть своїми словами, що таке метод об'єкту.
//Это свойства обьекта, представленные в виде функции.

// Який тип даних може мати значення властивості об'єкта?
//Любой

// Об'єкт це посилальний тип даних. Що означає це поняття?
// Переменная, которой присваивается обьект, хранит не сам обьект, а ссылку на участок памяти, в котором находится обьект.

createNewUser();

function createNewUser() {
  let userFirstName = prompt("Enter your first name");
  let userLastName = prompt("Enter our last name");

  let newUser = {
    _firstName: userFirstName,
    _lastName: userLastName,
    getLogin: function () {
      return (this._firstName.charAt(0) + this._lastName).toLowerCase();
    },
  };

  Object.defineProperty(newUser, "setFirstName", {
    set: function (value) {
      this._firstName = value;
    },
  });

  Object.defineProperty(newUser, "setLastName", {
    set: function (value) {
      this._lastName = value;
    },
  });

  return newUser;
}

let newUser = createNewUser();

console.log(newUser);

console.log(newUser.getLogin());
